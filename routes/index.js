const express = require('express');
const router = express.Router();
const Frases = require('../models/frases');
const speech = require('@google-cloud/speech');
const fs = require('fs');
const http = require('http');
const url_ = require('url');
const path = require('path');
var config = require('./../config/key');

/*
el servicio solo reconoce archivos de audio .flac, .ogg, .arm, .raw, .wav
*/

router.post('/audio-to-text', function(req, res, next) {
    //obtengo el parametro url que recibo
    var url = req.body.url;

    //inicializo el cliente de google speech to text con la key que me brinda google cloud
    const client = new speech.SpeechClient({
        keyFilename: config.KEY
    });
    
    if(url !== null || url !== ''){
      //obtengo el nombre del archivo, y la extension para validar que sean tipos de datos soportados
      var parsed = url_.parse(url);
      const fileName = __dirname +'/resources/'+path.basename(parsed.path);
      var extension = path.extname(parsed.path);
      
      if(extension === '.flac' ||  extension === '.ogg' || extension === '.arm' 
      || extension === '.raw' || extension === '.wav'){

        //descargo el archivo de audio
        var req = http.get(url, function(response){
          //obtengo el status code para saber si fue exitoso la descarga
          if(response.statusCode === 200){
            //guardo el archivo temporal que obtuve de la url
            var file_ = fs.createWriteStream(fileName);
            response.pipe(file_);
        
            file_.on('finish', function(){
              file_.close();

              //obtengo el archivo temporal y lo decodifico
              var file = fs.readFileSync(fileName);
              const audioBytes = file.toString('base64');
        
              //configuro las variables necesarias para el servicio de google cloud
              const audio = {
                content: audioBytes
              };
              const config = {
                enableWordTimeOffsets: true,
                languageCode: 'es-CO'
              };

              if(extension==='.ogg'){
                config.encoding = "OGG_OPUS";
                config.sampleRateHertz = 16000;
              }else if(extension==='.amr'){
                config.encoding = "AMR";
                config.sampleRateHertz = 8000;
              }else if(extension==='.raw' || extension==='.wav'){
                config.encoding = "LINEAR16";
                config.sampleRateHertz = 16000;
              }

              const request = {
                audio: audio,
                config: config
              };
        
              //hago el llamado con el cliente
              client
                .recognize(request)
                .then(data => {
                  const response = data[0];
                  const transcription = response.results
                    .map(result => result.alternatives[0].transcript)
                    .join(' ');
                  if(transcription === ''){
                    res.send({message:"no se pudo reconocer texto en el audio"});
                  }else{
                    //si recibo algun texto creo el objeto y lo guardo en la bd mongo
                    var frases = new Frases({
                        url: url,
                        texto: transcription,
                        palabras: transcription.split(" ")
                    });
                
                    frases.save(function(err){
                        if(err){        
                            res.send(err);
                        }else{
                            //borro el archivo temporal y envio el mensaje de exito
                            fs.unlinkSync(fileName);
                            res.send({message:"Enviado correctamente"});
                        }
                    });
                  }
                })
                .catch(err => {
                    //envio error desconocido
                    res.send({message:"error desconocido"});
                }); 
            });
          }else if(response.statusCode === 404){
            //envio error por url no encontrada
            res.send({message:"no se encontro el archivo"});
          }else{
            //envio por url no valida
            res.send({message:"la url no es correcta"});
          }
        });
      }else if(extension === ''){
        //envio error por no encontrarse ningun recurso
        res.send({message:'no se encuentra recurso'});
      }else{
        //envio error por tipo de dato no soportado
        res.send({message:"el archivo que envia no es soportado (archivos de audio soportados: .flac, .ogg, .arm, .raw, .wav)"});
      }
    }else{
      //envio por url vacia o nula
      res.send({message:'debe ingresar una url'});
    }
});

module.exports = router;
